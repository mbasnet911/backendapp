<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;

class APIRegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password'=> 'required',
            'dob' => 'date_format:Y-m-d|before:today|nullable',
            'country' => 'string|max:255',
            'profession' => 'string|max:255'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $userQuery = User::where('email', $request->get('email'));
        if ($user = $userQuery->first()) {
        	$data['status'] = 'fail';
            $data['msg'] = 'Email Address already exists';
            $response=Response::json($data);
            return $response;
        }

        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'dob' => $request->get('dob'),
            'country' => $request->get('country'),
            'profession' => $request->get('profession')
        ]);
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        
        return Response::json(compact('token'));
    }
}
